# A.V.Chizhov, June 2023. The program realizes micro- and macroscopic Markov models of sodium channels.
# The user must choose one of the models (Which_Model), the number of channels (N),
# set stimulation parameters(t_end and Stim_V) and the time step (dt):

# Choose model: 1 - [Kahlig, Misra, Jr 2006]            (21 states, 37 x 2 transitions);
#               2 - [Vandenberger and Bezanilla 1991]   ( 9 states,  9 x 2 transitions);
#               3 - 3-state model                       ( 3 states,  3 transitions)
#               4 - mutant R1648H [Kahlig, Misra, Jr 2006] (same as for Model 1)
Which_Model = 4
N = 1000    # Number of channels in microscopic model
t_end = 50  # Total simulation time in ms
dt = 0.025  # Time step in ms

def Stim_V(t :float):
    # Stimulus - VC-step
    if (t < 20) or (t > 40):  # ms
        V = -120 # mV
    else:
        V = -10 # mV
    return V

#***********************************************************************************************************************
import numpy as np
import matplotlib.pyplot as plt

# Initiate plotting
fig, ax = plt.subplots(nrows=4, figsize=(9, 10))
t_arr = np.zeros(int(t_end/dt))
V_arr = np.copy(t_arr)
x0_arr = np.copy(t_arr)
x1_arr = np.copy(t_arr)
x2_arr = np.copy(t_arr)
x3_arr = np.copy(t_arr)

# Rates and states from [Kahlig, Misra, Jr 2006]
Rates_Kahlig_Misra_Jr_2006 = [
    # n,  from, to,   alpha,                                  beta
    ["","","","",""],
    ["1", "C4", "C3", "77.149 * np.exp(V / 16.056) + 1.3181", " 5.63 * np.exp(-V / 47.619) + 2.6"],
    ["2", "C3", "C2", "48.115 * np.exp(V / 16.12) + 0.8", " 6.3137 * np.exp(-V / 73.692)"],
    ["3", "C2", "C1", "39.634 * np.exp(V / 44.236) + 5.3197", " 6.4559 * np.exp(-V / 28.947)"],
    ["4", "C1", "O", "4.25 * np.exp(V / 43.478) + 0.76", " 3.8617 * np.exp(-V / 44.028)"],
    ["5", "O", "F0", "5.2639 * np.exp(V / 31.779) + 3.8922e-4", " 0.033189 * np.exp(V / 11.34) + 0.22802"],
    ["6", "F0", "F0L", "0.4437 * np.exp(V / 48.1)", " 0.007 * np.exp(V / 5e4)"],
    ["7", "C4", "FC4", "3.0e-8 * np.exp(-V / 5e5)", " 4.592e-4 * np.exp(-V / 25.873) + 0.01957"],
    ["8", "C3", "FC3", "0.003 * np.exp(-V / 52.632)", " 5.8e-10 * np.exp(-V / 6.41) + 8.87e-4"],
    ["9", "C2", "FC2", "0.1888 * np.exp(-V / 50.582)", " 3.642e-4 * np.exp(-V / 16.276)"],
    ["10", "C1", "FC1", "0.8635 * np.exp(-V / 121.181)", " 3.1e-6 * np.exp(-V / 10.101)"],
    ["11", "FC4", "FC3", "0.02 * np.exp(V / 5e4)", " 0.0186 * np.exp(-V / 36.456)"],
    ["12", "FC3", "FC2", "0.0098053 * np.exp(V / 10.258) + 0.03", " 0.00546 * np.exp(-V / 33.445)"],
    ["13", "FC2", "FC1", "0.24 * np.exp(V / 9.09)", " 0.0082 * np.exp(-V / 20.92)"],
    ["14", "FC1", "F0L", "0.19 * np.exp(V / 15.151)", " 0.022 * np.exp(-V / 25.641)"],
    ["15", "C4", "SC4", "1.23285e-6 * np.exp(V / 47.772)", " 4.70479e-8 * np.exp(-V / 13.263)"],
    ["16", "C3", "SC3", "1.23285e-6 * np.exp(V / 47.772)", " 5.01079e-8 * np.exp(-V / 14.472)"],
    ["17", "C2", "SC2", "1.23285e-6 * np.exp(V / 47.772)", " 5.3367e-8 * np.exp(-V / 15.924)"],
    ["18", "C1", "SC1", "1.23285e-6 * np.exp(V / 47.772)", " 5.3367e-8 * np.exp(-V / 15.924)"],
    ["19", "O", "S0", "1.23285e-6 * np.exp(V / 47.772)", " 5.3367e-8 * np.exp(-V / 15.924)"],
    ["20", "FC4", "SFC4", "1.23285e-6 * np.exp(V / 47.772)", " 3.51119e-8 * np.exp(-V / 9.555)"],
    ["21", "FC3", "SFC3", "1.23285e-6 * np.exp(V / 47.772)", " 4.32876e-8 * np.exp(-V / 11.943)"],
    ["22", "FC2", "SFC2", "5.49e-4 * np.exp(V / 104.932)", " 3.65143e-8 * np.exp(-V / 9.926)"],
    ["23", "FC1", "SFC1", "1.23285e-6 * np.exp(V / 47.772)", " 9.38931e-8 * np.exp(-V / 158.697)"],
    ["24", "F0L", "SF0L", "5e-4 * np.exp(V / 74.627)", " 5.97425e-5 * np.exp(-V / 63.111)"],
    ["25", "SC4", "SC3", "0.385745 *np.exp(V / 16.056) + 0.00659", " 0.02815 * np.exp(-V / 47.619) + 0.013"],
    ["26", "SC3", "SC2", "0.240575 * np.exp(V / 16.12) + 0.004", " 0.0315685 * np.exp(-V / 73.692)"],
    ["27", "SC2", "SC1", "0.19817 * np.exp(V / 44.236) + 0.026599", " 0.0322795 * np.exp(-V / 28.947)"],
    ["28", "SC1", "S0", "0.02125 * np.exp(V / 43.478) + 0.0038", " 0.0193085 * np.exp(-V / 44.028)"],
    ["29", "S0", "SF0L", "0.026769 * np.exp(V / 32.146)", " 2.6567e-4 * np.exp(V / 50) + 8.8025e-4"],
    ["30", "SC4", "SFC4", "1.5e-10 * np.exp(-V / 5e5)", " 2.296e-6 * np.exp(-V / 25.873) + 9.785e-5"],
    ["31", "SC3", "SFC3", "1.5e-5 * np.exp(-V / 52.632)", " 2.9e-12 * np.exp(-V / 6.41) + 4.435e-6"],
    ["32", "SC2", "SFC2", "9.44e-4 * np.exp(-V / 50.582)", " 1.821e-6 * np.exp(-V / 16.276)"],
    ["33", "SC1", "SFC1", "0.0043175 * np.exp(-V / 121.181)", " 1.55e-8 * np.exp(-V / 10.101)"],
    ["34", "SFC4", "SFC3", "1e-4 * np.exp(V / 5e4)", " 9.3e-5 * np.exp(-V / 36.456)"],
    ["35", "SFC3", "SFC2", "4.90265e-5 * np.exp(V / 10.258) + 1.5e-4", " 2.73e-5 * np.exp(-V / 33.445)"],
    ["36", "SFC2", "SFC1", "1.2e-3 * np.exp(V / 9.091)", " 4.1e-5 * np.exp(-V / 20.921)"],
    ["37", "FSC1", "SF0L", "9.5e-4 * np.exp(V / 15.152)", " 1.1e-4 * np.exp(-V / 25.641)"]
]
StateNames_Kahlig_Misra_Jr_2006 = [
    "F0",
    "O",
    "C1",
    "C2",
    "C3",
    "C4",
    "S0",
    "SC1",
    "SC2",
    "SC3",
    "SC4",
    "F0L",
    "FC1",
    "FC2",
    "FC3",
    "FC4",
    "SF0L",
    "SFC1",
    "SFC2",
    "SFC3",
    "SFC4"
]
# mutant R1648H: modified beta in the transition 6:
Rates_Kahlig_Misra_Jr_2006_mutant_R1648H = Rates_Kahlig_Misra_Jr_2006
Rates_Kahlig_Misra_Jr_2006_mutant_R1648H[6][4] = " 0.043 * np.exp(V / 5e4)"

# Rates and states from [Vandenberger and Bezanilla 1991], in 1/ms
Rates_Vandenberger_Bezanilla_1991 = [
    # n,  from, to,   alpha,                                  beta
    ["","","","",""],
    ["1", "C1", "C2", "16.609 * np.exp(1.50*0.22 * V / 24)", "      0.971 * np.exp(-1.50*0.78 * V / 24)"],
    ["2", "C2", "C3", "16.609 * np.exp(1.50*0.22 * V / 24)", "      0.971 * np.exp(-1.50*0.78 * V / 24)"],
    ["3", "C3", "C4", "16.609 * np.exp(1.50*0.22 * V / 24)", "      0.971 * np.exp(-1.50*0.78 * V / 24)"],
    ["4", "C4", "C5", " 5.750 * np.exp(0.42*0.99 * V / 24)", "      4.325 * np.exp(-0.42*0.01 * V / 24)"],
    ["5", "I4", "I5", " 5.750 * np.exp(0.42*0.99 * V / 24)", "      4.325 * np.exp(-0.42*0.01 * V / 24)"],
    ["6", "C5", "O",  "15.669 * np.exp(1.91*0.75 * V / 24)", "      1.361 * np.exp(-1.91*0.25 * V / 24)"],
    ["7", "I5", "I",  "15.669 * np.exp(1.91*0.75 * V / 24)", "      1.361 * np.exp(-1.91*0.25 * V / 24)"],
    ["8", "O",  "I",  "0.432  * np.exp(0.91*0.001* V / 24)", "      0.004 * np.exp(-0.91*0.999* V / 24)"],
    ["9", "C4", "I4", "0.770  * np.exp(0.91*0.001* V / 24)", "770/432*0.004*np.exp(-0.91*0.999* V / 24)"]
]
StateNames_Vandenberger_Bezanilla_1991 = [
    "I",
    "O",
    "C5",
    "C4",
    "C3",
    "C2",
    "C1",
    "I4",
    "I5"
]

# Rates and states of test 3-state model given below, in 1/ms
Rates_3_state_model = [
    # n,  from, to,   alpha,                                  beta
    ["","","","",""],
    ["1", "C", "O", "0.2 / (1 + np.exp((-25 - V) / 5))", "  0"],
    ["2", "O", "I", "1.0", "  0"],
    ["3", "I", "C", "0.2", "  0"]
]
StateNames_3_state_model = [
    "I",
    "O",
    "C"
]

match Which_Model:
    case 1:
        Rates = Rates_Kahlig_Misra_Jr_2006
        StateNames = StateNames_Kahlig_Misra_Jr_2006
    case 2:
        Rates = Rates_Vandenberger_Bezanilla_1991
        StateNames = StateNames_Vandenberger_Bezanilla_1991
    case 3:
        Rates = Rates_3_state_model
        StateNames = StateNames_3_state_model
    case 4:
        Rates = Rates_Kahlig_Misra_Jr_2006_mutant_R1648H
        StateNames = StateNames_Kahlig_Misra_Jr_2006


def rate(n, n_alpha_beta, V):
    return eval(Rates[n][n_alpha_beta].replace('V',str(V)))

def Find_transition_for_states(i,j):
    # Calculates the number of the corresponding rate and the places of alpha and beta in the list of Rates
    si = StateNames[i]
    sj = StateNames[j]
    for k in range(len(Rates)):
        if ((Rates[k][1] == si) and (Rates[k][2] == sj)):
            return k,3,4
            exit()
        elif ((Rates[k][2] == si) and (Rates[k][1] == sj)):
            return k,4,3
            exit()
    return 0, 0, 0

def Calculate_Matrices_Of_Rates(V,NStates):
    ka = np.zeros((NStates, NStates), dtype=float)
    kb = np.zeros((NStates, NStates), dtype=float)
    for i in range(NStates):
        for j in range(NStates):
            k, n_alpha, n_beta = Find_transition_for_states(i, j)
            if k > 0:
                ka[i,j] = rate(k, n_alpha, V)       #1/ms
                kb[i,j] = rate(k, n_beta,  V)       #1/ms
    return ka, kb

def OneStepOfMarkov(X,V,dt,NStates):
    AA = np.zeros((NStates, NStates), dtype=float)
    BB = np.zeros(NStates, dtype=float)
    # Transition matrices
    ka, kb = Calculate_Matrices_Of_Rates(V,NStates)
    # Backward Euler method with matrices: AA * X = B
    for i in range(len(X)-1):
        AA[i,i] = 1/dt
        for j in range(len(X)):
            if i != j:
                AA[i,i] = AA[i,i] + ka[i,j]
                AA[i,j] = -kb[i,j]
        BB[i] = 1/dt * X[i]
    for j in range(len(X)):
        AA[len(X)-1,j] = 1
    BB[len(X)-1] = 1
    # Solve linear system
    X = np.linalg.solve(AA, BB)
    return X

#***********************************************************************************************************************
# Microscopic model
#***********************************************************************************************************************
NStates = len(StateNames)

# Set initial conditions
initial_state = np.zeros(N) + 2   # All channels start in state 2

# Initialize arrays to store states and time points
states = [initial_state]
channels_in_state1 = np.zeros(int(t_end/dt))

# Simulate the process
for nt in range(1,len(t_arr)):
    t = nt * dt
    current_state = states[-1].copy()  # Get the current state

    # Stimulus - VC-step
    V = Stim_V(t)

    # Transition matrices
    ka, kb = Calculate_Matrices_Of_Rates(V,NStates)

    # Loop over each channel and update its state
    for ip in range(N):  # ip - number of particle
        i = int(current_state[ip])
        # Play random number
        rn = np.random.rand() / dt
        row = ka[i,:] - rn
        positive_elements = row[row > 0]  # Filter positive elements in the row
        if positive_elements.size > 0:
            j = np.where(row == np.min(positive_elements))[0][0]
            current_state[ip] = j   # new state

    # Update current time and states
    states.append(current_state)
    t_arr[nt] = t
    # Count the number of channels in state 1 at each time point
    channels_in_state1[nt] = np.sum(current_state == 1)

# Convert the list of states into an array
states = np.array(states)

# Plot stimulation
ax[0].set_title('Voltage step')
for nt, t in enumerate(t_arr):
    V_arr[nt] = Stim_V(t)
ax[0].plot(t_arr, V_arr)
ax[0].set_xlabel('Time (s)')
ax[0].set_ylabel('V (mV)')

# Plot the evolution of states for the selected channel
ax[1].set_title('Evolution of states for few channels')
ax[1].plot(t_arr, states[:, 0], '-')
ax[1].plot(t_arr, states[:, 1], '-')
ax[1].plot(t_arr, states[:, 2], '-')
ax[1].plot(t_arr, states[:, 3], '-')
ax[1].plot(t_arr, states[:, 4], '-')
ax[1].set_xlabel('Time (ms)')
ax[1].set_ylabel('State')

# Plot the number of channels in state 1 versus time
ax[2].set_title('Number of open channels')
ax[2].plot(t_arr, channels_in_state1, '-')
ax[2].set_xlabel('Time (ms)')
ax[2].set_ylabel('Number of Channels in State 1')


#***********************************************************************************************************************
# MACROscopic model
#***********************************************************************************************************************

NStates = len(StateNames)
X = np.zeros(NStates, dtype=float)

# Initial conditions
X[2]=1
V = -70 #mV
X = OneStepOfMarkov(X,V,888,NStates)

# Time loop
for nt in range(0,len(t_arr)):
    t_arr[nt] = dt*nt
    # Stimulus - VC-step
    V = Stim_V(t_arr[nt])
    #*************************************
    X = OneStepOfMarkov(X, V, dt, NStates)
    #*************************************

    # Save for plotting
    x0_arr[nt] = X[0]
    x1_arr[nt] = X[1]
    x2_arr[nt] = X[2]

ax[3].set_title('Macro model')
ax[2].plot(t_arr, x1_arr*N)
ax[3].plot(t_arr, x0_arr, label='x0 - Inactivation ' +StateNames[0])
ax[3].plot(t_arr, x1_arr, label='x1 - Open '         +StateNames[1], marker='.')
ax[3].plot(t_arr, x2_arr, label='x2 - Closed '       +StateNames[2])
ax[3].set_xlabel('Time (ms)')
ax[3].set_ylabel('x0, x1, x2, units')
ax[3].legend()

plt.subplots_adjust(hspace=0.5)
plt.show()




# Below is the initial realization of micro- and macro- models for 3-states.

#***********************************************************************************************************************
# Microscopic 3-state model
#***********************************************************************************************************************
# 1 - Open
# 2 - Inactivated
# 3 - Closed

N = 1000

# Set initial conditions
initial_state = np.zeros(N) + 3   # All channels start in state 2

# Initialize arrays to store states and time points
states = [initial_state]
#t_arr = [0.0]
k31_arr = [0.0]

# Simulate the process
for nt in range(1,len(t_arr)):
    t = nt * dt
    # Stimulus - VC-step
    V = Stim_V(t)

    current_state = states[-1].copy()  # Get the current state

    # Define the transition rates (in Hz)
    k31 = 200 / (1 + np.exp((-25 - V) / 5))
    k12 = 1000
    k23 = 200

    k31_arr.append(k31)

    # Calculate transition probabilities
    p31 = k31 * dt/1000
    p12 = k12 * dt/1000
    p23 = k23 * dt/1000

    # Loop over each channel and update its state
    for i in range(N):
        if current_state[i] == 1:  # If channel i is in state 1
            if np.random.rand() < p12:  # Transition to state 2
                current_state[i] = 2
        elif current_state[i] == 2:
            if np.random.rand() < p23:  # Transition to state 3
                current_state[i] = 3
        else:  # If channel i is in state 3
            if np.random.rand() < p31:  # Transition to state 1
                current_state[i] = 1

    # Update current time and states
    states.append(current_state)
    t_arr[nt] = t

# Convert the list of states into an array
states = np.array(states)

# Count the number of channels in state 1 at each time point
channels_in_state1 = np.sum(states == 1, axis=1)

# Plot the number of channels in state 1 versus time
ax[2].set_title('Number of open channels')
ax[2].plot(t_arr, channels_in_state1, '-')
ax[2].set_xlabel('Time (s)')
ax[2].set_ylabel('Number of Channels in State 1')

# Plot the evolution of states for the selected channel
ax[1].set_title('Evolution of states for few channels')
ax[1].plot(t_arr, states[:, 0], '-')
ax[1].plot(t_arr, states[:, 1], '-')
ax[1].plot(t_arr, states[:, 2], '-')
ax[1].plot(t_arr, states[:, 3], '-')
ax[1].plot(t_arr, states[:, 4], '-')
ax[1].set_xlabel('Time (ms)')
ax[1].set_ylabel('State')

ax[0].set_title('Rate')
ax[0].plot(t_arr, k31_arr)
ax[0].set_xlabel('Time (ms)')
ax[0].set_ylabel('k31, units')
#plt.subplots_adjust(hspace=0.5)
#plt.show()

#***********************************************************************************************************************
# MACROscopic 3-state model
#***********************************************************************************************************************
x3=1.0
x2=0.0
x1=0.0
for nt in range(0,len(t_arr)):
    t = dt*nt
    # Stimulus - VC-step
    V = Stim_V(t)
    # Define the transition rates (in Hz)
    k31 = 200 / (1 + np.exp((-25 - V) / 5))
    k12 = 1000
    k23 = 200

    x1 = x1 + dt/1000*(-k12*x1 + k31*x3)
    x2 = x2 + dt/1000*( k12*x1 - k23*x2)
    x3 = 1 - x2 - x1

    x1_arr[nt] = x1
    x2_arr[nt] = x2
    x3_arr[nt] = x3

ax[3].set_title('Macro model')
ax[2].plot(t_arr, x1_arr*N)
ax[3].plot(t_arr, x1_arr, label='x1, 3st.')
ax[3].plot(t_arr, x2_arr, label='x1, 3st.')
ax[3].plot(t_arr, x3_arr, label='x1, 3st.')
ax[3].set_xlabel('Time (ms)')
ax[3].set_ylabel('x1, units')
ax[3].legend()

plt.subplots_adjust(hspace=0.5)
plt.show()
